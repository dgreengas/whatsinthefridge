import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsProvider with ChangeNotifier {
  String _units;
  SettingsProvider() {
    loadPreferences();
  }

  //getters
  String get units => _units;

  //setters
  void setUnits(String units) {
    this._units = units;
    savePreferences();
    notifyListeners();
  }

  savePreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('units', _units);
  } 

  loadPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String units = prefs.getString('units');
    setUnits( units != null ? units : 'Imperial');
  }
}
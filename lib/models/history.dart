import 'package:whatsinthefridge/models/location.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class History {
  History(
      {
      this.historyId,
      this.purchaseDate,
      this.expirationDate,
      this.quantity,
      this.unitOfMeasure,
      this.status,
      this.statusDate,
      this.percentUsed,
      this.location,
      this.purchasePrice});
  final String historyId;
  final DateTime purchaseDate;
  final DateTime expirationDate;
  final int quantity;
  final String unitOfMeasure;
  final String status;
  final DateTime statusDate;
  final int percentUsed;
  final Location location;
  final int purchasePrice;

  factory History.fromMap(String id, Map<String, dynamic> data) {
    if (data == null) {
      return null;
    }
    try {
      final String historyId  = id;
      final DateTime purchaseDate = data['purchaseDate'] != null
          ? DateTime.fromMillisecondsSinceEpoch(
              (data['purchaseDate'] as Timestamp).millisecondsSinceEpoch)
          : null;
      final DateTime expirationDate = data['expirationDate'] != null
          ? DateTime.fromMillisecondsSinceEpoch(
              (data['expirationDate'] as Timestamp).millisecondsSinceEpoch)
          : null;
      final DateTime statusDate = data['statusDate'] != null
          ? DateTime.fromMillisecondsSinceEpoch(
              (data['statusDate'] as Timestamp).millisecondsSinceEpoch)
          : null;
      final int quantity = data['quantity'];
      final String unitOfMeasure = data['unitOfMeasure'];
      final String status = data['status'];
      final int percentUsed = data['percentUsed'];
      final Location location = data['location'] != null ? Location.fromMap(data['location']) : null;
      final int purchasePrice = data['purchasePrice'];


      if (quantity == null && purchaseDate == null) {
        return null;
      }
      return History(historyId: historyId, purchaseDate: purchaseDate, expirationDate: expirationDate, 
      status: status, statusDate: statusDate, quantity: quantity, unitOfMeasure: unitOfMeasure,
      percentUsed: percentUsed, location: location, purchasePrice: purchasePrice);
    } catch ( e) {
        print(e.toString());
        print(data.toString());
        return null;
    }
  }

  Map<String, dynamic> toMap() {
    return {
      'id': historyId,
      'purchaseDate': Timestamp.fromDate(purchaseDate),
      'expirationDate': Timestamp.fromDate(expirationDate),
      'statusDate': Timestamp.fromDate(statusDate),
      'quantity': quantity,
      'location': location?.toMap(),
      'status': status,
      'unitOfMeasure': unitOfMeasure,
      'percentUsed': percentUsed,
      'purchasePrice': purchasePrice,
    };
  }
}

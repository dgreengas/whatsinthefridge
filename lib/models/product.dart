import 'package:flutter/material.dart';
import 'package:whatsinthefridge/models/history.dart';
import 'package:whatsinthefridge/models/tag.dart';

class Product {
  Product(
      {
      @required this.ownerUID,
      this.id,
      this.name,
      this.description,
      this.imageUrl,
      this.upc,
      this.productHomepage}): assert(ownerUID != null);

  final String id;
  final String name;
  final String description;
  List<History> _history;
  final String imageUrl;
  List<Tag> _tags;

  final List<String> upc;
  final String productHomepage;
  final String ownerUID; //use for authorization

  set history(List<History> history) {
    this._history = history;
  }

  List<History> get history => this._history;
  set tags(List<Tag> tags) {
    this._tags = tags;
  }

  List<Tag> get tags => this._tags;
  factory Product.fromMap(String documentId, Map<String, dynamic> data) {
    if (data == null) {
      return null;
    }
    final String ownerUID = data['ownerUID'];
    final String name = data['name'];
    final String id = documentId;
    final String description = data['description'];
    final imageUrl = data['imageUrl'];
    final String productHomepage = data['productHomepage'];
    final List<dynamic> upc = data['upc'];
    /*final List<History> history = data['history'] != null
        ? data['history'].map((history) => History.fromMap(history))
        : null;*/
    /*final List<Tag> tags = data['tags'] != null
        ? data['tags'].map((tag) => Tag.fromMap(tag))
        : null;*/

    if (id == null) {
      return null;
    }

    return Product(
        ownerUID: ownerUID,
        name: name,
        id: id,
        description: description,
        upc: upc.cast<String>(),
        productHomepage: productHomepage,
        imageUrl: imageUrl);
  }

  Map<String, dynamic> toMap() {
    //history and tags need to be sub-collections
    /*
     'history': history.map((h) => h.toMap()),
     'tags': tags.map((t) => t.toMap()),
    */
    return {
      'ownerUID': ownerUID,
      'id': id,
      'name': name,
      'description': description,
      'upc': upc,
      'productHomepage': productHomepage,
      'imageUrl': imageUrl
    };
  }
}


import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';

@immutable
class User  {
  final FirebaseUser firebaseUser;
  User({this.firebaseUser}): assert (firebaseUser != null);

  get userId => this.firebaseUser.uid;
  get email => this.firebaseUser.email;
  get providerId => this.firebaseUser.providerId;
  get phoneNumber => this.firebaseUser.phoneNumber;


}
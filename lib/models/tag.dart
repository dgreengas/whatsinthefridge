import 'dart:ui';

class Tag {
  Tag({this.tagId, this.name, this.color});
  final String tagId;
  final Color color;
  final String name;

  factory Tag.fromMap(String id, Map<String, dynamic> data) {
    if (data == null) {
      return null;
    }
    final String tagId = id;
    final Color color = data['color'] != null ? Color(data['color']) : null;
    final String name = data['name'];
    /*if (tagId == null) {
      return null;
    }*/
    return Tag(tagId: tagId, name: name, color: color);
  }

  Map<String, dynamic> toMap() {
    return {
      'tagId': tagId,
      'name': name,
      'color': color != null ? color.value : null,
    };
  }
}

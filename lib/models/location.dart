class Location {
  Location({this.locationId, this.name, this.imageUrl});
  final String imageUrl;
  final String locationId;
  final String name;

  factory Location.fromMap(Map<String, dynamic> data) {
    if (data == null) {
      return null;
    }
    final String imageUrl = data['imageUrl'];
    final String locationId = data['locationId'];
    final String name = data['name'];
    if (locationId == null) {
      return null;
    }
    return Location(locationId: locationId, name: name, imageUrl: imageUrl);
  }

  Map<String, dynamic> toMap() {
    return {
      'locationId': locationId,
      'name': name,
      'imageUrl': imageUrl,
    };
  }
}

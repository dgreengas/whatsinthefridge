import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:whatsinthefridge/pages/wrapper.dart';
import 'package:whatsinthefridge/providers/settings_provider.dart';
import 'package:whatsinthefridge/services/firebase_auth_service.dart';
import 'package:whatsinthefridge/services/image_picker_service.dart';
//import 'package:whatsinthefridge/themes/green_theme.dart';
// import 'package:whatsinthefridge/services/firebase_storage_service.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application
  final String title = "What's in the fridge";
  @override
  Widget build(BuildContext context) {
    
    final FirebaseAuthService firebaseAuthService = FirebaseAuthService();
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
              create: (BuildContext context) => SettingsProvider()),
          
          Provider<ImagePickerService>(
            create: (_) => ImagePickerService(),
          ),
          StreamProvider.value(value: firebaseAuthService.onAuthStateChanged)
        ],
        child: MaterialApp(
          title: title,
          theme: ThemeData(
            primarySwatch: Colors.green,
            // Define the default brightness and colors.
            brightness: Brightness.light,
            primaryColor: Color.fromARGB(255, 0x41, 0xa0, 0x56),
            accentColor: Color.fromARGB(255, 0x3f, 0x7e, 0x49),

            // Define the default font family.
            fontFamily: 'Cambay',

            // Define the default TextTheme. Use this to specify the default
            // text styling for headlines, titles, bodies of text, and more.
            /*textTheme: TextTheme(
                headline:
                    TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
                title: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
                body1: TextStyle(fontSize: 14.0, fontFamily: 'Cambay'),
              )*/
          ),
          home: Wrapper(),
        ));
  }
}

import 'package:flutter/material.dart';

class GreenTheme {
  static ThemeData createGreenTheme(BuildContext context) {
    return Theme.of(context).copyWith(
      accentColor: Color(0x41a056),
      backgroundColor: Color(0xedf8f3),
      focusColor: Color(0x3f7e49),
      buttonColor: Color(0x3f7e49),
      canvasColor: Color(0xedf8f3),
      primaryColor: Color(0x3f7e49),
      primaryTextTheme: TextTheme(
        headline1: TextStyle(color: Colors.white,
        fontFamily: 'Cambay',
        fontWeight: FontWeight.w700,
        )
      )

    );
  }
}

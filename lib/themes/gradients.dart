import 'dart:ui';

import 'package:flutter/cupertino.dart';

class Gradients {

  const Gradients();

  static const Color loginGradientStart = const Color(0xFF3f7e49);
  static const Color loginGradientEnd = const Color(0xFFedf8f3);

  static const primaryGradient = const LinearGradient(
    colors: const [loginGradientStart, loginGradientEnd],
    stops: const [0.0, 1.0],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
  );
}
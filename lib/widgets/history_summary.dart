import 'package:flutter/material.dart';
import 'package:whatsinthefridge/models/history.dart';
import 'package:intl/intl.dart';

class HistorySummary extends StatelessWidget {
  final List<History> productHistory;
  const HistorySummary({Key key, this.productHistory}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0.0,
      color: Theme.of(context).backgroundColor,
      child: Column(
        children: <Widget>[
          _averagePriceWidget(),
          _lastPurchasedWidget(),
          _quantityInStock(),
          _earliestExpiration(),
          SizedBox(
            height: 30.0,
          ),
          _dummyChart()
        ],
      ),
    );
  }

  Widget _averagePriceWidget() {
    if (productHistory != null) {
      double averagePrice = 0.0;
      double sumPrice = 0.0;
      productHistory.forEach((hist) {
        sumPrice += hist.purchasePrice != null ? hist.purchasePrice : 0;
      });
      averagePrice = sumPrice / productHistory.length;
      return _contentCard(
          'Average Price', (averagePrice / 100).toStringAsFixed(2));
    } else {
      return _notAvailableCard();
    }
  }

  Widget _lastPurchasedWidget() {
    DateFormat df = DateFormat.yMd();
    if (productHistory != null) {
      List<DateTime> hist =
          productHistory.map<DateTime>((h) => h.purchaseDate).toList();
      hist.sort();
      hist = hist.reversed.toList();
      if (hist.length == 0) {
        return _notAvailableCard();
      }
      return _contentCard('Last Purchased', df.format(hist[0]));
    } else {
      return _notAvailableCard();
    }
  }

  Widget _quantityInStock() {
    if (productHistory != null) {
      double sumQuantity = 0.0;
      productHistory.forEach((hist) {
        sumQuantity +=
            hist?.quantity != null ? hist.quantity * (100 - (hist.percentUsed ?? 0))/ 100 : 0;
      });
      return _contentCard(
          'Quantity In Stock', (sumQuantity).toStringAsFixed(0));
    } else {
      return _notAvailableCard();
    }
  }

  Widget _earliestExpiration() {
    DateFormat df = DateFormat.yMd();
    print('_earliestExpiration_tart');
    if (productHistory != null) {
      List<DateTime> hist =
          productHistory.map<DateTime>((h) => h.expirationDate).toList();
      hist.sort();
      print('_earliestExpiration_end');
      if (hist.length == 0) {
        return _notAvailableCard();
      }
      return _contentCard('Earliest Expiration', df.format(hist[0]));
    } else {
      return _notAvailableCard();
    }
  }

  Widget _dummyChart() {
    return CircularProgressIndicator(
      value: 12,
      strokeWidth: 12.0,
    );
  }

  Widget _notAvailableCard() {
    return Card(
      elevation: 10.0,
      color: Colors.white,
      child: Text(
        'Not available',
        style: TextStyle(fontStyle: FontStyle.italic),
      ),
    );
  }

  Widget _contentCard(String label, String value) {
    return Card(
      elevation: 5.0,
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text('$label: '),
          Text(value),
        ],
      ),
    );
  }
}

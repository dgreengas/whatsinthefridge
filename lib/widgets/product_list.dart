import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:whatsinthefridge/models/product.dart';

import 'package:whatsinthefridge/widgets/product_list_item.dart';

class ProductList extends StatelessWidget {
  const ProductList({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Product> productList = Provider.of<List<Product>>(context);

    return ListView.builder(
      itemBuilder: (BuildContext context, int item) {
        return ProductListItem(product: productList[item]);
      },
      itemCount: productList != null ? productList.length : 0,
    );
  }
}

import 'package:flutter/material.dart';
import 'package:whatsinthefridge/models/history.dart';
import 'package:intl/intl.dart';
class HistoryDetail extends StatelessWidget {
  final List<History> productHistory;

  const HistoryDetail({Key key, @required this.productHistory})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    DateFormat df = DateFormat.yMd();
    return Card(
      elevation: 0.0,
      color: Theme.of(context).backgroundColor,
      child: ListView.builder(itemBuilder: (BuildContext context, int item) {
        return ListTile(
            title: Text(df.format( productHistory[item].statusDate)));
      },
      itemCount: productHistory.length,),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:whatsinthefridge/models/product.dart';
//import 'package:whatsinthefridge/models/history.dart';
import 'package:timeago/timeago.dart' as timeago;
//import 'package:whatsinthefridge/pages/detail_page.dart';
import 'package:whatsinthefridge/pages/detail_page_concept.dart';
import 'package:whatsinthefridge/widgets/tag_widget.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class ProductListItem extends StatelessWidget {
  final Product product;
  ProductListItem({Key key, @required this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (product == null) {
      return ListTile(title: Text('null'));
    }
    return Slidable(
      actionPane: SlidableDrawerActionPane(),
      actionExtentRatio: 0.25,
      child: Container(
          color: Colors.transparent,
          child: Card(
            elevation: 8.0,
            margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            child: Container(
              decoration:
                  BoxDecoration(color: Theme.of(context).backgroundColor),
              child: ListTile(
                leading: Container(
                  padding: EdgeInsets.only(right: 12.0),
                  decoration: new BoxDecoration(
                      border: new Border(
                          right: new BorderSide(
                              width: 1.0, color: Colors.white24))),
                  child: Hero(
                    tag: product.imageUrl ?? (product.name + 'image'),
                    child: CircleAvatar(
                        maxRadius: 32.0,
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(32.0),
                            child: product.imageUrl != null ? Image.network(product.imageUrl) : Text(product.name[0]) )),
                  ),
                ),
                title: Hero(
                    tag: product.name,
                    child: Text(
                      product.name,
                      style: TextStyle(
                          color: Colors.black45, fontWeight: FontWeight.bold),
                    )),
                subtitle: _createExpiryWidget(),
                trailing: Icon(Icons.keyboard_arrow_right,
                    color: Colors.black45, size: 30.0),
                onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            DetailPageConcept(product: product))),
              ),
            ),
          )),
      actions: <Widget>[
        IconSlideAction(
          caption: 'Use',
          color: Colors.lightGreen,
          icon: FontAwesomeIcons.checkSquare,
          onTap: () => print("Used"),
        ),
        IconSlideAction(
          caption: 'Buy Again',
          color: Colors.deepOrange,
          icon: FontAwesomeIcons.shoppingBag,
          onTap: () => print("Buy"),)
      ],
      secondaryActions: <Widget>[
        IconSlideAction(
            caption: 'Throw Away',
            color: Colors.redAccent,
            icon: FontAwesomeIcons.trash,
            onTap: () => print("Throw Away"))
      ],
    );
  }

  Widget _createTagWidget() {
    return Wrap(
      runAlignment: WrapAlignment.spaceBetween,
      runSpacing: 5.0,
      spacing: 5.0,
      children: product.tags != null
          ? product.tags.map((tag) => TagWidget(tag: tag)).toList()
          : <Widget>[Container()],
    );
  }

  Widget _createExpiryWidget() {
    List<DateTime> hist = product.history != null
        ? product.history.map<DateTime>((h) => h.expirationDate).toList()
        : List<DateTime>();
    hist.sort();
    if (hist.length > 0) {
      print("""Expiry Raw: ${hist[0]}
       -- Expiry Parsed ${timeago.format(hist[0])} 
       -- Exipiry Future ${timeago.format(DateTime.now().add(Duration(days: 200)))}""");
      return Text("Expires: ${timeago.format(hist[0], allowFromNow: true)}");
    } else {
      return Container();
    }
  }
}

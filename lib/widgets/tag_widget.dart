import 'package:flutter/material.dart';
import 'package:whatsinthefridge/models/tag.dart';

class TagWidget extends StatelessWidget {
  final Tag tag;
  TagWidget({Key key, @required this.tag})
      : assert(tag != null),
        super(key: key);
  @override
  Widget build(BuildContext context) {
    return Chip(
      label: Text(
        tag.name,
      ),
      backgroundColor: tag.color,
      labelStyle: TextStyle(fontSize: 10.0),
      padding: EdgeInsets.only(left: 1.0, right: 1.0),
    );
  }
}

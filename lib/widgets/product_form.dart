import 'dart:io';

import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_formfield/flutter_datetime_formfield.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:whatsinthefridge/models/user.dart';
import 'package:whatsinthefridge/services/firebase_storage_service.dart';
import 'package:whatsinthefridge/services/image_picker_service.dart';
import 'package:whatsinthefridge/utils/clip_circle.dart';
import 'package:whatsinthefridge/services/firestore_service.dart';
import 'package:whatsinthefridge/models/history.dart';
import 'package:whatsinthefridge/models/product.dart';
import 'package:uuid/uuid.dart';

class ProductForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ProductFormState();
  }
}

class ProductFormState extends State<ProductForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _barcodeEditingController =
      TextEditingController();
  String barcode;
  String _productName;
  String _productUrl;
  int _quantity = 0;
  double _price = 0.0;
  String _unit;
  DateTime _purchaseDate = DateTime.now().toLocal();
  DateTime _expirationDate;
  File _productImage;
  User user;
  @override
  Widget build(BuildContext context) {
    DateFormat df = DateFormat.yMd();
    _expirationDate ??= _purchaseDate.add(Duration(days: 30));

    user = Provider.of<User>(context);

    return Provider<FirebaseStorageService>(
        create: (_) => FirebaseStorageService(uid: "productimages"),
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              Center(
                child: InkWell(
                  child: CircleAvatar(
                    maxRadius: 32,
                    child: Visibility(
                      visible: (_productImage != null),
                      child: _productImage != null
                          ? ClipOval(
                              clipper: ClipCircle(radius: 32),
                              child: Image.file(_productImage))
                          : Container(),
                    ),
                  ),
                  onTap: () async {
                    final imagePicker =
                        Provider.of<ImagePickerService>(context, listen: false);
                    final File imageFile =
                        await imagePicker.pickImage(source: ImageSource.camera);
                    setState(() {
                      this._productImage = imageFile;
                    });
                    //fileState.setState(() => imageFile);
                  },
                ),
              ),
              TextFormField(
                controller: _barcodeEditingController,
                decoration: const InputDecoration(
                  icon: Icon(FontAwesomeIcons.barcode),
                  hintText: 'Scan the barcode',
                  labelText: 'Product UPC',
                ),
                onTap: _scan,
              ),
              TextFormField(
                key: Key('product.name'),
                decoration: const InputDecoration(
                  icon: Icon(FontAwesomeIcons.breadSlice),
                  hintText: 'Enter the Product Name',
                  labelText: 'Product Name',
                ),
                onSaved: (String value) => this._productName = value,
                validator: (String value) =>
                    value.isEmpty ? 'Please enter a product name' : null,
              ),
              TextFormField(
                keyboardType: TextInputType.url,
                key: Key('product.productHomepage'),
                decoration: const InputDecoration(
                  icon: Icon(FontAwesomeIcons.link),
                  hintText: 'Enter the url of the product details',
                  labelText: 'Product Url',
                ),
                onSaved: (String value) => this._productUrl = value,
              ),
              TextFormField(
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  icon: Icon(FontAwesomeIcons.calculator),
                  hintText: "Enter the quantity",
                  labelText: "Quantity",
                ),
                onSaved: (String value) => this._quantity = int.tryParse(value),
              ),
              TextFormField(
                keyboardType: TextInputType.text,
                initialValue: "PC",
                decoration: InputDecoration(
                  icon: Icon(FontAwesomeIcons.square),
                  hintText: "Enter the unit",
                  labelText: "Unit",
                ),
                onSaved: (String value) => this._unit = value,
              ),
              TextFormField(
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  icon: Icon(FontAwesomeIcons.calculator),
                  hintText: "Enter the price",
                  labelText: "Price",
                ),
                onSaved: (String value) => this._price = double.tryParse(value),
              ),
              DateTimeFormField(
                initialValue: _purchaseDate,
                label: "Date of Purchase",
                onlyDate: true,
                formatter: df,
                validator: (DateTime dateTime) {
                  if (dateTime == null) {
                    return "Purchase Date is Required";
                  }
                  return null;
                },
                onSaved: (DateTime dateTime) => _purchaseDate = dateTime,
              ),
              DateTimeFormField(
                initialValue: _expirationDate,
                label: "Date of Expiration",
                onlyDate: true,
                formatter: df,
                validator: (DateTime dateTime) {
                  if (dateTime == null) {
                    return "Expiration Date Required";
                  }
                  return null;
                },
                onSaved: (DateTime dateTime) => _expirationDate = dateTime,
              ),
              SizedBox(
                height: 100.0,
              ),
              RaisedButton(
                onPressed: _submitForm,
                elevation: 20.0,
                color: Colors.green,
                child: Text('Save'),
              ),
              /*FormButton(
            buttonType: ButtonType.Save,
            onPressed: _submitForm,
          )*/
            ],
          ),
        ));
  }

  /*Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _purchaseDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != _purchaseDate)
      setState(() {
        _purchaseDate = picked;
      });
  }*/

  void _submitForm() async {
    if (!_formKey.currentState.validate()) {
      return;
    }
    _formKey.currentState.save();
    //onsaved gets called for each method
    // Construct a product and save

    FirestoreService firestoreService = FirestoreService();
    final firestoreStorage = Provider.of<FirebaseStorageService>(
        _formKey.currentContext,
        listen: false);
    print("Product Image ${_productImage == null}");
    String productImageUrl;
    if (_productImage != null) {
      productImageUrl = await firestoreStorage.uploadImage(file: _productImage);
    }
    Product product = Product(
        ownerUID: user.userId,
        id: Uuid().v4(),
        name: this._productName,
        upc: [this.barcode],
        imageUrl: productImageUrl,
        productHomepage: _productUrl);
    List<History> history = [
      History(
          historyId: Uuid().v4(),
          quantity: this._quantity,
          purchasePrice: (this._price * 100.0).toInt(),
          unitOfMeasure: this._unit,
          purchaseDate: this._purchaseDate,
          expirationDate: this._expirationDate,
          statusDate: DateTime.now())
    ];
    product.history = history;
    firestoreService.addProduct(product: product);

    Navigator.of(_formKey.currentContext).pop();
  }

  Future _scan() async {
    try {
      String barcode = await BarcodeScanner.scan();
      setState(() {
        this.barcode = barcode;
        this._barcodeEditingController.text = barcode;
      });
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          //TODO: Fix this to use the controller
          this.barcode = "The user did not grant permission";
        });
      } else {
        setState(() {
          this.barcode = "Unkown error $e";
        });
      }
    } on FormatException {
      setState(() {
        //user cancelled
        this.barcode = "";
      });
    } catch (e) {
      setState(() {
        this.barcode = "Unknown error $e";
      });
    }
  }
}

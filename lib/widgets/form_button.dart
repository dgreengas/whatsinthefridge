import 'package:flutter/material.dart';

enum ButtonType {Save, Cancel, Submit}
class FormButton extends StatelessWidget {
  final ButtonType buttonType;
  final Function onPressed;

  final Color colorSave = Colors.green;
  final Color colorCancel = Colors.red;

  const FormButton({Key key, @required this.buttonType, @required this.onPressed}) : 
    assert(buttonType != null), assert(onPressed != null),  super(key: key);

  @override
  Widget build(BuildContext context) {

    return RaisedButton(
      onPressed: this.onPressed(),
      color: _getButtonColor(),
      child: _buildButtonLabel(),
    );
  }

  Color _getButtonColor() {
     switch(this.buttonType) {
       case ButtonType.Save:
       case ButtonType.Submit:
         return colorSave;
         break;
       case ButtonType.Cancel:
         return colorCancel;
         break;
       default:
         return Colors.white;
         break;
     }
  }

  Widget _buildButtonLabel() {
    switch(this.buttonType) {
      case ButtonType.Save:
        return Text("Save");
        break;
      case ButtonType.Submit:
        return Text("Submit");
        break;
      case ButtonType.Cancel:
        return Text("Cancel");
        break;
      default:
        return Text("Click");
        break; 
    }
  }
}
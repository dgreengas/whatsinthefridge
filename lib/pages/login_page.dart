import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:whatsinthefridge/services/firebase_auth_service.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:whatsinthefridge/utils/progress_page.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => new _LoginPageState();
}

//with SingleTickerProviderStateMixin
class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();

  String _email = "";
  String _password = "";
  String _errorMessage = "";
  bool _loading = false;
  int _mode = 0; //0->Login, 1->Register

  @override
  Widget build(BuildContext context) {
    return _loading
        ? ProgressPage()
        : Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('images/login.jpg'),
                    fit: BoxFit.fill,
                    colorFilter: ColorFilter.mode(
                        Colors.black.withOpacity(0.4), BlendMode.darken))),
            child: SafeArea(
                child: Scaffold(
                    backgroundColor: Colors.transparent,
                    body: Form(
                      key: _formKey,
                      child: SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        padding: EdgeInsets.symmetric(horizontal: 20.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            SizedBox(height: 30.0),
                            Text(
                              "You must sign in to continue",
                              style: TextStyle(
                                  fontSize: 26.0, color: Colors.white),
                            ),
                            SizedBox(
                              height: 30.0,
                            ),
                            Card(
                              color: Colors.white.withOpacity(0.85),
                              margin: EdgeInsets.symmetric(horizontal: 15.0),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 8.0),
                                    child: TextFormField(
                                      initialValue: _email,
                                      decoration: InputDecoration(
                                          labelText: "Enter your email",
                                          prefixIcon: Icon(_mode == 1
                                              ? Icons.person_add
                                              : Icons.person_outline)),
                                      keyboardType: TextInputType.emailAddress,
                                      onChanged: (value) =>
                                          setState(() => _email = value),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 8.0),
                                    child: TextFormField(
                                      initialValue: _password,
                                      decoration: InputDecoration(
                                        prefixIcon: Icon(Icons.lock),
                                        labelText: "Enter your password",
                                        fillColor: Colors.white,
                                        filled: true,
                                        hintStyle: GoogleFonts.roboto(
                                          fontSize: 12,
                                          color: Color(0xff8492a6),
                                        ),
                                      ),
                                      obscureText: true,
                                      validator: (value) {
                                        if (value.length < 6) {
                                          return "Your password must be a lest 6 characters";
                                        }
                                        return null;
                                      },
                                      onChanged: (value) =>
                                          setState(() => _password = value),
                                    ),
                                  ),
                                  RaisedButton(
                                    elevation: 10.0,
                                    child:
                                        Text(_mode == 0 ? "Login" : "Register"),
                                    color: Colors.green,
                                    onPressed: () async {
                                      if (_formKey.currentState.validate()) {
                                        setState(() {
                                          _loading = !_loading;
                                        });
                                        final FirebaseAuthService authService =
                                            FirebaseAuthService();
                                        if (_mode == 0) {
                                          final dynamic user =
                                              await authService.signIn(
                                                  provider:
                                                      SignInProvider.Email,
                                                  user: _email,
                                                  password: _password);
                                          if (user is String) {
                                            setState(() {
                                              _errorMessage = user;
                                              _loading = !_loading;
                                            });
                                          }
                                        } else {
                                          //register
                                          final dynamic response =
                                              await authService.register(
                                                  user: _email,
                                                  password: _password);
                                          if (response is String) {
                                            //error
                                            setState(() {
                                              _errorMessage = response;
                                              _loading = !_loading;
                                            });
                                          }
                                        }
                                      }
                                    },
                                  ),
                                  Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: [
                                        InkWell(
                                          child: Text(
                                            "Forgot My Password",
                                            style: TextStyle(
                                                decoration:
                                                    TextDecoration.underline),
                                          ),
                                          onTap: () => {},
                                        ),
                                        FlatButton.icon(
                                          icon: Icon(_mode == 0
                                              ? Icons.person_add
                                              : Icons.person_outline),
                                          label: Text(_mode == 0
                                              ? "Register"
                                              : "Login"),
                                          onPressed: () {
                                            setState(() {
                                              _mode = _mode == 0 ? 1 : 0;
                                            });
                                          },
                                        )
                                      ])
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                            Visibility(
                                visible: (_errorMessage.length > 0),
                                child: Card(
                                  color: Colors.white,
                                  child: Text(_errorMessage,
                                      style: TextStyle(
                                        fontSize: 16.0,
                                        color: Colors.red,
                                      )),
                                )),
                          ],
                        ),
                      ),
                    ))));
  }
}

/*
                             style: GoogleFonts.roboto(
                            fontSize: 14,
                            color: Color(0xff8492a6),
                          ),                             


                              */

/*


        */

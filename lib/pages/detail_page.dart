import 'package:flutter/material.dart';
import 'package:whatsinthefridge/models/product.dart';

class DetailPage extends StatelessWidget {
  final Product product;

  DetailPage({@required this.product}) : assert(product != null);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          expandedHeight: 150,
          floating: true,
          backgroundColor: Theme.of(context).appBarTheme.color,
          pinned: true,
          snap: false,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () => Navigator.of(context).pop(),
              ),
              Hero(
                tag: product.imageUrl,
                child: CircleAvatar(
                  maxRadius: 16.0,
                  child: Image.network(product.imageUrl),
                ),
              ),
              Hero(
                  tag: product.name,
                  child: Text(
                    product.name,
                    style: Theme.of(context).primaryTextTheme.headline1,
                  ))
            ],
          ),
          flexibleSpace: Container(
            color: Theme.of(context).appBarTheme.color,
            height: 120,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Hero(
                  tag: 'empty',
                  child: CircleAvatar(
                    maxRadius: 100,
                    child: Image.network(product.imageUrl),
                  ),
                ),
                Hero(
                    tag: 'empty2',
                    child: Text(
                      product.name,
                      style: Theme.of(context)
                          .primaryTextTheme
                          .headline1
                          .copyWith(fontSize: 36.0),
                    ))
              ],
            ),
          ),
        ),
        SliverGrid(
          gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 200.0,
            mainAxisSpacing: 10.0,
            crossAxisSpacing: 10.0,
            childAspectRatio: 4.0,
          ),
          delegate: SliverChildBuilderDelegate(
            (BuildContext context, int index) {
              return Container(
                alignment: Alignment.center,
                color: Colors.teal[100 * (index % 9)],
                child: Text('Grid Item $index'),
              );
            },
            childCount: 20,
          ),
        ),
        SliverFixedExtentList(
          itemExtent: 50.0,
          delegate: SliverChildBuilderDelegate(
            (BuildContext context, int index) {
              return Container(
                alignment: Alignment.center,
                color: Colors.lightBlue[100 * (index % 9)],
                child: Text('List Item $index'),
              );
            },
          ),
        ),
      ],
    ));
  }
}

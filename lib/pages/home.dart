import 'package:flutter/material.dart';
import 'package:whatsinthefridge/pages/add_product_page.dart';
import 'package:whatsinthefridge/pages/settings.dart';
import 'package:whatsinthefridge/services/firebase_auth_service.dart';
import 'package:whatsinthefridge/widgets/product_list.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("What's in the fridge"),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => SettingsPage()));
              }),
          IconButton(
            icon: Icon(Icons.filter_list),
            onPressed: () => {},
          ),
          IconButton(
            icon: Icon(Icons.lock_open),
            onPressed: () {
              final FirebaseAuthService authService = FirebaseAuthService();
              authService.signOut();
            }

          )
        ],
        centerTitle: false,
      ),
      body: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("images/bg.jpg"), fit: BoxFit.fill)),
          child: ProductList()),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => Navigator.of(context).push(MaterialPageRoute(
            builder: (BuildContext context) => AddProductPage())),
      ),
      bottomNavigationBar: BottomNavigationBar(items: [
        BottomNavigationBarItem(
            icon: Icon(Icons.fingerprint), title: Text('Print')),
        BottomNavigationBarItem(
            icon: Icon(Icons.show_chart), title: Text('History'))
      ]),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:whatsinthefridge/providers/settings_provider.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var settingsProvider = Provider.of<SettingsProvider>(context);
    
    return Scaffold(
      appBar: AppBar(
        title: Text('Settings'),
        
      ),
      body: Column(
        children: <Widget>[
          SizedBox(height: 25.0,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Text('Default Units'),
                DropdownButton<String>(
                  value: settingsProvider.units,
                  items: <String>['Imperial', 'Metric']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                        value: value, child: Text(value));
                  }).toList(),
                  onChanged: (String value) {
                    settingsProvider.setUnits(value);
                  },
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

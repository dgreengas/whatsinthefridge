import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:whatsinthefridge/models/user.dart';
import 'package:whatsinthefridge/pages/home.dart';
import 'package:whatsinthefridge/pages/login_page.dart';
import 'package:whatsinthefridge/services/firestore_service.dart';

/// Wrapper Page to determine if the home page
/// or login page should be shown
class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final User currentUser = Provider.of<User>(context);
    final FirestoreService firestoreService = FirestoreService(ownerUID: currentUser.userId);
    return currentUser == null
        ? LoginPage()
        : MultiProvider(providers: [
            StreamProvider.value(
              value: firestoreService.productListStream(),
            ),
          ], child: HomePage());
  }
}

import 'package:flutter/material.dart';
import 'package:page_indicator/page_indicator.dart';
import 'package:whatsinthefridge/models/product.dart';
import 'package:whatsinthefridge/widgets/history_detail.dart';
import 'package:whatsinthefridge/widgets/history_summary.dart';

class DetailPageConcept extends StatelessWidget {
  final Product product;

  const DetailPageConcept({Key key, @required this.product})
      : assert(product != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
        body: Stack(
      children: <Widget>[
        Container(
            height: double.infinity,
            width: double.infinity,
            color: Theme.of(context).backgroundColor),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Flexible(
                flex: 1,
                child: Container(
                  color: Theme.of(context).backgroundColor,
                )),
            Flexible(
              flex: 4,
              child: _productDetail(context),
            )
          ],
        ),
        _productImage(context),
        _navBarButton(context)
      ],
    ));
  }

  Widget _navBarButton(BuildContext context) {
    return Positioned(
      left: 8.0,
      top: 36.0,
      child: Material(
          color: Theme.of(context).backgroundColor,
          child: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back, color: Colors.white),
          )),
    );
  }

  Widget _productImage(BuildContext context) {
    return Positioned(
      top: 60.0,
      left: (MediaQuery.of(context).size.width / 2) - 64.0,
      child: Hero(
        
        tag: product.imageUrl ?? (product.name + 'image'),
        child: CircleAvatar(
            maxRadius: 64.0,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(64.0),
              child: product.imageUrl != null ? Image.network(product.imageUrl) : Text(product.name[0]),
            )),
      ),
    );
  }

  Widget _productDetail(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 60.0),
          Hero(
              tag: product.name,
              child: Text(product.name,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal,
                      fontSize: 24.0,
                      color: Colors.black))),
          SizedBox(
            height: 32.0,
          ),
          Center(
            
                child: Container(
                    height: 400.0,
                    width: MediaQuery.of(context).size.width*.80,
                    color: Colors.transparent,
                    child: PageIndicatorContainer(
                      child: PageView(
                        
                        children: <Widget>[
                          HistoryDetail(productHistory: product.history),
                          HistorySummary(productHistory: product.history),
                        ],
                      ),
                      indicatorColor: Colors.white,
                      indicatorSelectorColor: Colors.green,
                      align: IndicatorAlign.bottom,
                      length: 2,
                    )),

                /*Material(
                      borderRadius: BorderRadius.circular(20.0),
                      elevation: 20.0,
                      child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20.0),
                            shape: BoxShape.rectangle,
                            color: Theme.of(context).backgroundColor,
                          ),
                          height: 400.0,
                          width: double.infinity,
                          padding: EdgeInsets.all(6.0),
                          child: Text('details'))) */
              ),
              
        ],
      ),
    );
  }
}

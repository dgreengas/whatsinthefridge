import 'package:flutter/material.dart';
import 'package:whatsinthefridge/widgets/product_form.dart';

class AddProductPage extends StatelessWidget {
  AddProductPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add a Product'),
      ),
      body: ProductForm(),
    );
  }
}
/*
Product product = Product(id: Uuid().toString(), name: 'Green Lettuce', upc: ['112345678'], description: 'The greenest of lettuce', imageUrl: 'https://source.unsplash.com/random/128x128', );
        List<History> history = [History( location: Location(locationId: 'veg_drawer', name: 'Vegetable Drawer', imageUrl:'https://source.unsplash.com/random/32x32' ),percentUsed: 0, quantity: 1, unitOfMeasure: 'PC', purchasePrice: 299,purchaseDate: DateTime.now(), statusDate: DateTime.now(), expirationDate: DateTime.now().add(Duration(days: 15)))];
        product.history = history;
        List<Tag> tags = [ 
            Tag(name: 'Vegetables', color: Colors.brown)
        ];
        product.tags = tags;
        
        firestoreService.addProduct(product: product);
      }*/

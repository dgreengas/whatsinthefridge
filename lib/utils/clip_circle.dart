
import 'package:flutter/material.dart';

class ClipCircle extends CustomClipper<Rect> {
  final double radius;


  ClipCircle({this.radius=32});

  @override
  Rect getClip(Size size) {
    return Rect.fromLTWH(0, 0, this.radius, this.radius);
  }

  @override
  bool shouldReclip(CustomClipper<Rect> oldClipper) {
    return false;
  }
  
}

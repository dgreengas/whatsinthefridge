
import 'package:flutter/material.dart';

class ProgressPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.green[100],
      child: Center(
        child: CircularProgressIndicator(
          backgroundColor: Colors.green[400],
        ),
      ),
    );
  }
}
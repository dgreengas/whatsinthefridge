import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:whatsinthefridge/services/firestore_path.dart';
import 'package:flutter/foundation.dart';
import 'package:uuid/uuid.dart';

class FirebaseStorageService {
  final String uid;
  FirebaseStorageService({@required this.uid}): assert(uid != null);
  /// Upload an avatar from file
  Future<String> uploadImage({
    
    @required File file,
  }) async  {
      if (file == null) {
        print("file is null");
        return null;
      }
      final String pathUuid =  Uuid().v1();
      return upload(
        
        file: file,
        path: FirestorePath.product(pathUuid) + '/image.png',
        contentType: 'image/png',
      );
  }
  /// Generic file upload for any [path] and [contentType]
  Future<String> upload({
    
    @required File file,
    @required String path,
    @required String contentType,
  }) async {
    print('uploading to: $path');
    final firebaseStorage = FirebaseStorage(
        storageBucket: 'gs://whatsinthefridge-55f52.appspot.com/'
    );
    final storageReference = firebaseStorage.ref().child(path);
    final uploadTask = storageReference.putFile(
        file, StorageMetadata(contentType: contentType));
    final snapshot = await uploadTask.onComplete;
    if (snapshot.error != null) {
      print('upload error code: ${snapshot.error}');
      throw snapshot.error;
    }
    // Url used to download file/image
    final downloadUrl = await snapshot.ref.getDownloadURL();
    print('downloadUrl: $downloadUrl');
    return downloadUrl;
  }
}

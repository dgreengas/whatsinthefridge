import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:whatsinthefridge/models/product.dart';
import 'package:whatsinthefridge/models/tag.dart';
import 'package:whatsinthefridge/models/history.dart';
//import 'package:whatsinthefridge/services/firestore_path.dart';
import 'package:flutter/foundation.dart';
import 'package:uuid/uuid.dart';

class FirestoreService {
  //final String uid;
  final String ownerUID;
  final Firestore firestoreInstance = Firestore.instance;
  FirestoreService({this.ownerUID});

  Future<void> addProduct({
    @required Product product,
  }) async {
    String documentId;
    if (product.id == null) {
      documentId = Uuid().v4();
    } else {
      documentId = product.id;
    }
    //final path = FirestorePath.product(uid);
    try {
      //final reference = firestoreInstance.collection('products');
      await firestoreInstance.document('/products/$documentId').setData(product.toMap());
      //DocumentReference productDocument = await reference.add(product.toMap());
      //await referencedata.document(documentId).setData(product.toMap());
      //DocumentReference productDocument = firestoreInstance.document("products/$documentId");
      product.tags?.forEach((tag) => firestoreInstance.document('/products/$documentId/tags/${tag.tagId}')
          .setData(tag.toMap()));
      product.history?.forEach((history) =>  firestoreInstance.document('/products/$documentId/history/${history.historyId}')
          .setData(history.toMap()));
    } catch (e) {
      print(e.toString());
    }
    //await reference.add(product.toMap());
  }

  Stream<List<Product>> productListStream() {
    final products = firestoreInstance.collection('products').where("ownerUID",isEqualTo: this.ownerUID) .orderBy('name');
    return products
        .snapshots()
        .map((snapshot) => snapshot.documents.map((document) {
              Product product =
                  Product.fromMap(document.documentID, document.data);
              DocumentReference productReference = firestoreInstance
                  .collection('products')
                  .document(document.documentID);
              productReference
                  .collection('tags')
                  .snapshots()
                  .forEach((snapshot) {
                product.tags = snapshot.documents
                    .map((tagDocument) =>
                        Tag.fromMap(tagDocument.documentID, tagDocument.data))
                    .toList();
              });
              productReference
                  .collection('history')
                  .snapshots()
                  .forEach((snapshot) {
                product.history = snapshot.documents
                    .map((historyDocument) => History.fromMap(
                        historyDocument.documentID, historyDocument.data))
                    .toList();
              });
              return product;
            }).toList());
  }

  Future<void> addHistory(productId, History history) async {
    final product =
        firestoreInstance.collection('products').document(productId);
    if (product != null) {
      product
          .collection('history')
          .document(history.historyId)
          .setData(history.toMap());
    }
  }

  Future<void> addTag(productId, Tag tag) async {
    final product =
        firestoreInstance.collection('products').document(productId);
    if (product != null) {
      product.collection('tags').document(tag.tagId).setData(tag.toMap());
    }
  }

  Future<void> removeTag(productId, Tag tag) async {
    final product =
        firestoreInstance.collection('products').document(productId);
    if (product != null) {
      product.collection('tags').document(tag.tagId).delete();
    }
  }
}

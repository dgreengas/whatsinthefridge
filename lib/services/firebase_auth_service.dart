import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:whatsinthefridge/models/user.dart';

enum SignInProvider {
  Google,
  Email,
  Phone,
  Anonymous,
  GitHub,
  Facebook,
  Twitter,
}

class FirebaseAuthService {
  final _firebaseAuth = FirebaseAuth.instance;
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth _auth = FirebaseAuth.instance;

  User _userFromFirebase(FirebaseUser user) {
    return user == null ? null : User(firebaseUser: user);
  }

  Stream<User> get onAuthStateChanged {
    return _firebaseAuth.onAuthStateChanged.map(_userFromFirebase);
  }

  Future<User> signInAnonymously() async {
    final authResult = await _firebaseAuth.signInAnonymously();
    return _userFromFirebase(authResult.user);
  }

  Future<dynamic> register({String user, String password}) async {
     try {
       final AuthResult result = await _firebaseAuth.createUserWithEmailAndPassword(email: user, password: password);
       return _userFromFirebase(result.user);

     } catch (e) {
       print(e.toString());
       return e.message;
     }
  }

  Future<dynamic> signIn(
      {SignInProvider provider,
      String user,
      String password,
      String phoneNumber}) async {
    switch (provider) {
      case SignInProvider.Google:
        final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
        final GoogleSignInAuthentication googleAuth =
            await googleUser.authentication;

        final AuthCredential credential = GoogleAuthProvider.getCredential(
          accessToken: googleAuth.accessToken,
          idToken: googleAuth.idToken,
        );
        final FirebaseUser user =
        (await _auth.signInWithCredential(credential)).user;
        final IdTokenResult token = await user.getIdToken();
        if (token != null) {
          return User(firebaseUser: await _auth.currentUser());
        }

        break;
      case SignInProvider.Email:
        try {
           AuthResult result = await _auth.signInWithEmailAndPassword(email: user, password: password);
           return result.user;
        } catch (e) {
          print(e.toString());
          return e.message;
        }
        break;
      case SignInProvider.Phone:
        // TODO: Handle this case.
        break;
      case SignInProvider.Anonymous:
        return signInAnonymously();
        break;
      case SignInProvider.GitHub:
        // TODO: Handle this case.
        break;
      case SignInProvider.Facebook:
        // TODO: Handle this case.
        break;
      case SignInProvider.Twitter:
        // TODO: Handle this case.
        break;
    }

    return null;
  }

  Future<void> signOut() async {
    return await _firebaseAuth.signOut();
  }
}
